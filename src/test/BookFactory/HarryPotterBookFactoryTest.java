package BookFactory;

import Books.BookName;
import Books.HarryPotterBook;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HarryPotterBookFactoryTest {

    private HarryPotterBookFactory harryPotterBookFactory;

    @Before
    public void setUp() throws Exception {
         harryPotterBookFactory = new HarryPotterBookFactory();
    }

    @Test
    public void shouldFetchThePhilosophersStone() throws Exception {
        HarryPotterBook thePhilosophersStone = harryPotterBookFactory.fetchBook(BookName.THE_PHILOSOPHERS_STONE);
        Assert.assertTrue(thePhilosophersStone.getBookName().equals(BookName.THE_PHILOSOPHERS_STONE));
    }

    @Test
    public void shouldFetchTheChamberOfSecrets() throws Exception {
        HarryPotterBook theChamberOfSecrets = harryPotterBookFactory.fetchBook(BookName.THE_CHAMBER_OF_SECRETS);
        Assert.assertTrue(theChamberOfSecrets.getBookName().equals(BookName.THE_CHAMBER_OF_SECRETS));
    }

    @Test
    public void shouldFetchThePrisonerOfAzkaban() throws Exception {
        HarryPotterBook theprisonerOfAzkaban = harryPotterBookFactory.fetchBook(BookName.THE_PRISONER_OF_AZKABAN);
        Assert.assertTrue(theprisonerOfAzkaban.getBookName().equals(BookName.THE_PRISONER_OF_AZKABAN));
    }

    @Test
    public void shouldFetchTheGobletOfFire() throws Exception {
        HarryPotterBook theGobletOfFire = harryPotterBookFactory.fetchBook(BookName.THE_GOBLET_OF_FIRE);
        Assert.assertTrue(theGobletOfFire.getBookName().equals(BookName.THE_GOBLET_OF_FIRE));
    }

    @Test
    public void shouldFetchTheOrderOfPhoenix() throws Exception {
        HarryPotterBook theOrderOfPhoenix = harryPotterBookFactory.fetchBook(BookName.THE_ORDER_OF_THE_PHOENIX);
        Assert.assertTrue(theOrderOfPhoenix.getBookName().equals(BookName.THE_ORDER_OF_THE_PHOENIX));
    }
}