package BookStore;

import BookFactory.HarryPotterBookFactory;
import Books.BookName;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PriceTest {

    private Price price;
    private ShoppingBasket shoppingBasket;
    private HarryPotterBookFactory harryPotterBookFactory;

    @Before
    public void setUp() throws Exception {
        price = new Price();
        shoppingBasket = new ShoppingBasket();
        harryPotterBookFactory = new HarryPotterBookFactory();
    }

    @Test
    public void shouldReturnOneBookWithNormalPrice() throws Exception {
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_PHILOSOPHERS_STONE));
        Double total = price.calculate(shoppingBasket);
        Assert.assertTrue(total.equals(8D));
    }

    @Test
    public void shouldReturnPriceWithFivePercentDiscountForTwoDifferentBooks() throws Exception {
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_PHILOSOPHERS_STONE));
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_CHAMBER_OF_SECRETS));

        Double total = price.calculate(shoppingBasket);

        Assert.assertTrue(total.equals(15.2D));
    }

    @Test
    public void shouldReturnPriceWithTenPercentDiscountForThreeDifferentBooks() throws Exception {

        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_PHILOSOPHERS_STONE));
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_CHAMBER_OF_SECRETS));
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_PRISONER_OF_AZKABAN));

        Double total = price.calculate(shoppingBasket);

        Assert.assertTrue(total.equals(21.6D));
    }

    @Test
    public void shouldReturnPriceWithTwentyPercentDiscountForFourDifferentBooks() throws Exception {
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_PHILOSOPHERS_STONE));
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_CHAMBER_OF_SECRETS));
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_PRISONER_OF_AZKABAN));
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_GOBLET_OF_FIRE));

        Double total = price.calculate(shoppingBasket);

        Assert.assertTrue(total.equals(25.6D));
    }

    @Test
    public void shouldReturnPriceWithTwentyFivePercentDiscountForFiveDifferentBooks() throws Exception {
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_PHILOSOPHERS_STONE));
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_CHAMBER_OF_SECRETS));
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_PRISONER_OF_AZKABAN));
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_GOBLET_OF_FIRE));
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_ORDER_OF_THE_PHOENIX));

        Double total = price.calculate(shoppingBasket);

        Assert.assertTrue(total.equals(30.0D));
    }

    @Test
    public void shouldReturnPriceWithMultipleDiscounts() throws Exception {
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_PHILOSOPHERS_STONE));
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_PHILOSOPHERS_STONE));


        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_CHAMBER_OF_SECRETS));
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_CHAMBER_OF_SECRETS));


        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_PRISONER_OF_AZKABAN));
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_PRISONER_OF_AZKABAN));

        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_GOBLET_OF_FIRE));
        shoppingBasket.add(harryPotterBookFactory.fetchBook(BookName.THE_ORDER_OF_THE_PHOENIX));

        Double total = price.calculate(shoppingBasket);

        Assert.assertTrue(total.equals(51.6D));
    }
}