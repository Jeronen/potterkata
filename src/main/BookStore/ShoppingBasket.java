package BookStore;

import Books.HarryPotterBook;

import java.util.ArrayList;
import java.util.List;

public class ShoppingBasket {
	private List<HarryPotterBook> books;

	void add(HarryPotterBook book){
		if(books == null){
			books = new ArrayList<>();
		}
		books.add(book);
	}

	public List<HarryPotterBook> getBooks() {
		return books;
	}
}
