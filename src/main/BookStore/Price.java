package BookStore;

import Books.BookName;
import Books.HarryPotterBook;

import java.util.ArrayList;
import java.util.List;

class Price {

	private Double  price = 0D;
	private int amountOfBooks = 0;

	Double calculate(ShoppingBasket shoppingBasket) {
		List<HarryPotterBook> books = shoppingBasket.getBooks();
		List<BookName> bookNames = new ArrayList<>();

		while (amountOfBooks < books.size()) {
            for (HarryPotterBook book : books) {
				if (!bookNames.contains(book.getBookName()) && !book.getBilled()) {
					bookNames.add(book.getBookName());
					book.setBilled(true);
                    amountOfBooks += 1;
                }
            }
            Double discountPercent = getDiscountPercent(bookNames.size());
            Double priceWithoutDiscount = bookNames.size() * 8D;
            Double percent = priceWithoutDiscount / 100;
            Double totalDiscount = percent * discountPercent;
            price += priceWithoutDiscount - totalDiscount;
            bookNames.clear();
        }
		return price;
	}

	private Double getDiscountPercent(int size){
		switch (size){
			case 0:
			case 1:
				return 0D;

			case 2: return 5D;
			case 3: return 10D;
			case 4: return 20D;
			case 5:	return 25D;
		}
		return 0D;
	}
}
