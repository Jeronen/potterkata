package BookFactory;

import Books.*;

public class HarryPotterBookFactory {

	public HarryPotterBook fetchBook(BookName bookName) {
		String name = bookName.name();
		switch (name){
			case "THE_PHILOSOPHERS_STONE":
				return new ThePhilosophersStone();
			case "THE_CHAMBER_OF_SECRETS":
				return new TheChamberOfSecrets();
			case "THE_PRISONER_OF_AZKABAN":
				return new ThePrisonerOfAzkaban();
			case "THE_GOBLET_OF_FIRE":
				return new TheGobletOfFire();
			case "THE_ORDER_OF_THE_PHOENIX":
				return new TheOrderOfThePhoenix();
			default:
				throw new UnsupportedOperationException();
		}
	}
}
