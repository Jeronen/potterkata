package Books;

public abstract class HarryPotterBook{
	BookName bookName;
	private Boolean billed = false;

	public BookName getBookName() {
		return bookName;
	}

	public Boolean getBilled() {
        return billed;
    }

    public void setBilled(Boolean billed) {
        this.billed = billed;
    }
}
